import firebase from 'react-native-firebase';

const config = {
    apiKey: "AIzaSyD_vSt2be22hAJdaQgEBybGPVtNbi2c0sA",
    appId: "1:159400275062:android:fea45f2747647ce6",
    authDomain: "sabi-d9b49.firebaseapp.com",
    databaseURL: "https://sabi-d9b49.firebaseio.com",
    projectId: "sabi-d9b49",
    storageBucket: "sabi-d9b49.appspot.com",
    messagingSenderId: "159400275062"
};

export const firebaseapp = firebase.initializeApp(config,"sabiapp");
