// @flow
import React from "react";
import { createSwitchNavigator, createStackNavigator, createDrawerNavigator } from "react-navigation";
import { StyleSheet, Dimensions } from "react-native";
import { Root } from "native-base";
import Login from "./container/LoginContainer";
import Home from "./container/HomeContainer";
import BlankPage from "./container/BlankPageContainer";
import Sidebar from "./container/SidebarContainer";
import AuthLoading from "./container/AuthLoadingContainer/index";
import History from "./container/HistoryContainer";
import About from "./container/AboutContainer";
import { Icon } from "native-base";
const SCREEN_WIDTH = Dimensions.get("window").width;

const Drawer = createDrawerNavigator(
	{
		Home: {
			screen: Home,
			navigationOptions: {
				title: "Trang chủ",
				drawerIcon: ({ tintColor }) => (
					<Icon
						name="home"
						style={styles.iconNav}
					/>
				),
			}
		},
		History: {
			screen: History,
			navigationOptions: {
				title: "Lịch sử",
				drawerIcon: ({ tintColor }) => (
					<Icon
						name="history"
						style={styles.iconNav}
					/>
				),
			}
		},
		BlankPage: {
			screen: BlankPage,
			navigationOptions: {
				title: "Thông tin cá nhân",
				drawerIcon: ({ tintColor }) => (
					<Icon
						name="account"
						style={styles.iconNav}
					/>
				),
			}
		},

		About: {
			screen: About,
			navigationOptions: {
				title: "Giới thiệu",
				drawerIcon: ({ tintColor }) => (
					<Icon
						name="lightbulb-on"
						style={styles.iconNav}
					/>
				),
			}
		}
	},
	{
		initialRouteName: "Home",
		drawerPosition: "left",
		drawerWidth: SCREEN_WIDTH * 0.8,
		contentComponent: props => <Sidebar {...props} />,
		contentOptions: {
			activeTintColor: "rgba(95,86,176,1)",
			itemsContainerStyle: {
				marginVertical: 0,
			},
			iconContainerStyle: {
				opacity: 1
			}
		},
		backBehavior: "initialRoute"
	}
);

const AppStack = createStackNavigator(
	{
		Home: { screen: Home },
		BlankPage: { screen: BlankPage },
		History: { screen: History },
		About: { screen: About },
		Drawer: { screen: Drawer },
	}, {
		initialRouteName: "Drawer",
		headerMode: "none",
	}
);

const AuthStack = createStackNavigator(
	{
		Login: { screen: Login }
	},
	{
		headerMode: "none",
	});
const App = createSwitchNavigator(
	{
		AuthLoading: { screen: AuthLoading },
		App: AppStack,
		Auth: AuthStack,
	},
	{
		initialRouteName: "AuthLoading",
		headerMode: "none",
	}
);

const styles = StyleSheet.create({
	iconNav: {
		color: "rgba(161,168,201,1)",
		fontSize: 24,
	}
});
export default () => (
	<Root>
		<App />
	</Root>
);
