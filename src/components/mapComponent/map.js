import React from "react";
import { View, Text } from "react-native";
import { Container } from "native-base";
import { connect } from 'react-redux';
import {
    getCurrentLocation,
    getInputData,
    toogleSearchResult,
    getAddressPredictions,
    getSelectedAddress,
    createSchedule,
    resetMapReducer
} from "../../container/MapContainer/reducer";
import MapContainer from "../../container/MapContainer/mapContainer";
import styles from "./styles";

class Map extends React.Component {

    componentDidMount() {
        this.props.getCurrentLocation();
    }

    render() {
        const region = {
            latitude: 10.771596,
            longitude: 106.700426,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421
        }
        return (

            <View style={styles.container}>
                {
                    this.props.region.latitude &&
                    <MapContainer
                        navigation={this.props.navigation}
                        region={this.props.region}
                        getInputData={this.props.getInputData}
                        toogleSearchResult={this.props.toogleSearchResult}
                        getAddressPredictions={this.props.getAddressPredictions}
                        resultTypes={this.props.resultTypes}
                        predictions={this.props.predictions}
                        getSelectedAddress={this.props.getSelectedAddress}
                        selectedAddress={this.props.selectedAddress}
                        distanceMatrix={this.props.distanceMatrix}
                        createSchedule={this.props.createSchedule}
                        resetMapReducer={this.props.resetMapReducer}
                        result={this.props.result}
                    />
                }
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        region: state.mapReducer.region,
        inputData: state.mapReducer.inputData || {},
        resultTypes: state.mapReducer.resultTypes || {},
        predictions: state.mapReducer.predictions || [],
        selectedAddress: state.mapReducer.selectedAddress || {},
        distanceMatrix: state.mapReducer.distanceMatrix,
        schedule: state.mapReducer.schedule || {},
        result: state.mapReducer.result
    }
}
const mapActionCreators = {
    getCurrentLocation,
    getInputData,
    toogleSearchResult,
    getAddressPredictions,
    getSelectedAddress,
    createSchedule,
    resetMapReducer
}

export default connect(mapStateToProps, mapActionCreators)(Map);