import React from "react";
import { Text, FlatList } from "react-native";
import { View, List, ListItem, Icon, Left, Body } from "native-base";

import styles from "./styles";

const SearchResult = ({ predictions, getSelectedAddress }) => {
    function handleSelectedAddress(placeID) {
        getSelectedAddress(placeID);
    }

    const _renderItem = (item) => {
        return (

            <ListItem onPress={() => handleSelectedAddress(item.placeID)} button avatar>
                <Left style={styles.leftContainer}>
                    <Icon style={styles.leftIcon} name="crosshairs-gps" />
                </Left>
                <Body>
                    <Text style={styles.primaryText}>{item.primaryText}</Text>
                    <Text style={styles.secondaryText}>{item.secondaryText}</Text>
                </Body>
            </ListItem>
        );
    }

    return (
        <View style={styles.searchResultsWrapper}>
            <List
                dataArray={predictions}
                // renderRow={(item) =>
                //     <View>
                //         <ListItem onPress={() => handleSelectedAddress(item.placeID)} button avatar>
                //             <Left style={styles.leftContainer}>
                //                 <Icon style={styles.leftIcon} name="crosshairs-gps" />
                //             </Left>
                //             <Body>
                //                 <Text style={styles.primaryText}>{item.primaryText}</Text>
                //                 <Text style={styles.secondaryText}>{item.secondaryText}</Text>
                //             </Body>
                //         </ListItem>
                //     </View>
                // }
                renderRow={(item) => _renderItem(item)}
            />
            {/* <FlatList
                data={predictions}
                keyExtractor={_keyExtractor}
                renderItem={_renderItem} /> */}

        </View>
    );
};
export default SearchResult;
