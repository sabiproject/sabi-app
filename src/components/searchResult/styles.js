import { Dimensions } from "react-native";
var width = Dimensions.get("window").width; //full width
const styles = {
    searchResultsWrapper:{
        top:180,
        position:"absolute",
        width:width,
        height:1000,
        backgroundColor:"#fff",
        opacity:0.9
    },
    primaryText:{
        fontWeight: "bold",
        color:"#373737",
        fontSize:12,
    },
    secondaryText:{
        fontStyle: "italic",
        color:"#7D7D7D",        
        fontSize:11,
    },
    leftContainer:{
        flexWrap: "wrap",
        alignItems: "flex-start",
        borderLeftColor:"#7D7D7D",
    },
    leftIcon:{
        fontSize:14,
        color:"#7D7D7D",
    },
    distance:{
        fontSize:12,
    }
};

export default styles;