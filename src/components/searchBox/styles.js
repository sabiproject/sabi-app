import { Dimensions } from "react-native";
var width = Dimensions.get("window").width; //full width
const styles = {
    searchBox: {
        top: 0,
        position: "absolute",
        width: width
    },
    inputWrapper: {
        marginLeft: 10,
        marginRight: 10,
        marginTop: 5,
        marginBottom: 0,
        backgroundColor: "#fff",
        opacity: 0.9,
        borderRadius: 5
    },
    secondInputWrapper: {
        marginLeft: 10,
        marginRight: 10,
        marginTop: 5,
        backgroundColor: "#fff",
        opacity: 0.9,
        borderRadius: 5
    },
    inputSearch: {
        fontSize: 12
    },
    label: {
        fontSize: 10,
        fontStyle: "italic",
        marginLeft: 10,
        marginTop: 5,
        marginBottom: 0
    }
};

export default styles;