import React from "react";
import { Text } from "react-native";
import { View, InputGroup, Input, Icon } from "native-base";

import styles from "./styles";

export class SearchBox extends React.Component {

    state = {
        selectedPickUp: this.props.selectedAddress.selectedPickUp,
        selectedDropOff: this.props.selectedAddress.selectedDropOff
    }

    componentDidUpdate(prevProps) {
        if (this.props.selectedAddress.selectedDropOff && prevProps.selectedAddress.selectedDropOff != this.props.selectedAddress.selectedDropOff) {
            this.setState({
                selectedDropOff: this.props.selectedAddress.selectedDropOff
            })
        }
        if (this.props.selectedAddress.selectedPickUp && prevProps.selectedAddress.selectedPickUp != this.props.selectedAddress.selectedPickUp) {
            this.setState({
                selectedPickUp: this.props.selectedAddress.selectedPickUp
            })
        }
        if (!this.props.selectedAddress && prevProps.selectedAddress) {
            this.setState({
                selectedDropOff: this.props.selectedAddress.selectedDropOff,
                selectedPickUp: this.props.selectedAddress.selectedPickUp
            })
        }
    }
    render() {
        const { getInputData, toogleSearchResult, getAddressPredictions, selectedAddress } = this.props;
        const { selectedPickUp, selectedDropOff } = selectedAddress || {};
        const handleInput = (key, val) => {
            if (key == "pickUp") {
                this.setState({
                    selectedPickUp: { name: val }
                })
            } else {
                this.setState({
                    selectedDropOff: { name: val }
                })
            }
            getInputData({
                key,
                value: val
            });
            getAddressPredictions();
        }
        return (
            <View style={styles.searchBox}>
                <View style={styles.inputWrapper}>
                    <Text style={styles.label}>Tôi đang ở</Text>
                    <InputGroup>
                        <Icon name="hospital-marker" size={15} color="rgba(95,86,176,1)" />
                        <Input
                            onFocus={() => toogleSearchResult("pickUp")}
                            style={styles.inputSearch}
                            placeholder="Điểm đi"
                            onChangeText={handleInput.bind(this, "pickUp")}
                            value={this.state.selectedPickUp && this.state.selectedPickUp.name}
                        />
                    </InputGroup>
                </View>
                <View style={styles.secondInputWrapper}>
                    <Text style={styles.label}>và tôi muốn đến</Text>
                    <InputGroup>
                        <Icon name="map-marker-radius" />
                        <Input
                            onFocus={() => { toogleSearchResult("dropOff"); }}
                            style={styles.inputSearch}
                            placeholder="Điểm đến"
                            onChangeText={handleInput.bind(this, "dropOff")}
                            value={this.state.selectedDropOff && this.state.selectedDropOff.name}
                        />
                    </InputGroup>
                </View>
            </View>
        );
    };
}
export default SearchBox;
