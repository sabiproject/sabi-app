import React from "react";
import { Text ,Picker} from "react-native";
import { View, InputGroup, Input, Icon, Badge, Button } from "native-base";

import DatePicker from "../datePicker";
import TimePicker from "../timePicker";
import moment from "moment";

import styles from "./styles";
import Alert from "../Alert";
import Spinner from "../spinner-laoding";

export default class BookDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showResult: false,
            loadingVisible: false,
            datetime: moment(),
            typeSharing: "nonbike",
            fare: 0
        }
    }
    componentDidUpdate(prevProps, prevState) {
        if (prevProps.result != this.props.result && !prevProps.result) {
            this.setState({
                loadingVisible: false,
                showResult: true
            })
        }
    }
    getTime = (time) => {
        const temp = this.state.datetime.format("L").toString();
        this.setState({ datetime: moment(temp + " " + time, "MM/DD/YYYY HH:mm") });
    };

    getDate = (date) => {
        const temp = this.state.datetime.format("LT").toString();
        this.setState({ datetime: moment(date + " " + temp, "MM/DD/YYYY HH:mm") });
    };
    closeAlertWhenFailure = () => {
        this.setState({
            showResult: false
        })
    }
    handleClose = () => {
        this.setState({
            showResult: false
        })
        this.props.resetMapReducer();
        // dispatch event reset reducer to continue schedule
    }
    handleOK = () => {
        this.setState({
            showResult: false
        })
        // dispatch event reset reducer and navigate to home        
        this.props.resetMapReducer();
        this.props.navigation.navigate("Home");
    }

    submitSchedule = () => {
        this.setState({ loadingVisible: true });
        const payload = {
            datetime: this.state.datetime.toJSON(),
            fare: this.state.fare,
            typeSharing: this.state.typeSharing
        }
        this.props.createSchedule(payload)
    }
    render() {
        const { distanceMatrix, createSchedule, result } = this.props;
        const data = [{ label: 'Orange', value: 'orange', key: 'orange', color: 'orange' }];

        return (
            <View style={styles.searchBox}>
                {
                    result ?
                        <Alert
                            show={this.state.showResult}
                            theme={"success"}
                            title={"Thành công"}
                            subtitle={"Lịch trình đã được thêm. Vui lòng đợi thông báo xác nhận từ đối tác"}
                            handleClose={this.handleClose}
                            handleOK={this.handleOK}
                            confirmText={"Trở về Trang chủ"}
                            cancelText="Tiếp tục đặt lịch mới"
                            themeCancel={"inverse"}
                            onRequestClose={() => { return; }}
                        ></Alert> :
                        <Alert
                            show={this.state.showResult}
                            theme={"danger"}
                            title={"Thất bại"}
                            subtitle={"Thêm lịch thất bại vui lòng thử lại"}
                            handleClose={this.closeAlertWhenFailure}
                            cancelText="Xong"
                            themeCancel={"inverse"}
                            onRequestClose={() => { return; }}
                        ></Alert>
                }
                <Spinner
                    modalVisible={this.state.loadingVisible}
                    animationType="fade"
                />
                <View style={styles.inputWrapper}>
                    <View>
                        <Text style={[styles.label, { paddingLeft: 5 }]}>Vào lúc</Text>
                        <View style={{ alignItems: "center", flexDirection: 'row', marginLeft: 5 }}>
                            <Icon name="clock" style={styles.styleIcon} />
                            <TimePicker time={this.state.datetime} getTime={this.getTime} />
                            <Icon name="calendar-clock" style={styles.styleIcon} />
                            <DatePicker time={this.state.datetime} getDate={this.getDate} />
                        </View>
                    </View>
                    <View style={{ justifyContent: "flex-start", alignItems: "flex-start" }}>
                        <Text style={styles.label}>khoảng</Text>
                        <View style={{ alignItems: "center", justifyContent: "flex-start", padding: 0, margin: 0, }}>
                            {distanceMatrix.rows.length > 0 ?
                                <Text>{distanceMatrix.rows[0].elements[0].distance.text}</Text> :
                                <Text>...</Text>
                            }
                        </View>
                    </View>
                    <View style={{ marginRight: 5 }}>
                        <Text style={styles.label}>trong</Text>
                        <View>
                            {distanceMatrix.rows.length > 0 ?
                                <Text>{distanceMatrix.rows[0].elements[0].duration.text}</Text> :
                                <Text>...</Text>
                            }
                        </View>

                    </View>


                </View>
                <View style={styles.secondInputWrapper}>
                    <View style={{ width: 110 }}>
                        <Text style={[styles.label, { paddingLeft: 5 }]}>hổ trợ xăng xe</Text>
                        <InputGroup>
                            <Icon name="currency-usd" style={styles.styleIcon} />
                            <Input
                                keyboardType="numeric"
                                maxLength={7}
                                onChangeText={(text) => { this.setState({ fare: text }) }}
                                style={[styles.inputSearch]}
                                placeholder="miễn phí"
                            />
                        </InputGroup>
                    </View>
                    <View>
                        <Text style={[styles.label]}>loại chuyến đi</Text>
                        <Picker
                            mode="dropdown"
                            iosHeader="Chọn loại chia sẽ"
                            iosIcon={<Icon name="arrow-dropdown-circle" style={{ color: "#007aff", fontSize: 25 }} />}
                            style={{ width: 150, fontSize: 10 }}
                            itemTextStyle={{ fontSize: 10 }}
                            textStyle={{ fontSize: 10 }}
                            selectedValue={this.state.typeSharing}
                            onValueChange={(value) => { this.setState({ typeSharing: value }) }}
                        >
                            <Picker.Item label="Có xe" value="bike" />
                            <Picker.Item label="Không có xe" value="nonbike" />
                        </Picker>
                    

                    </View>
                    <View>
                        <Text style={[styles.label]}> </Text>
                        <Button style={styles.btn} small onPress={() => { this.submitSchedule() }}>
                            <Text style={styles.btnText}>Tôi đã xong</Text>
                        </Button>
                    </View>


                </View>
            </View>
        );
    }
};
