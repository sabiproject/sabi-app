import { Dimensions } from "react-native";
var width = Dimensions.get("window").width; //full width
const styles = {
    searchBox: {
        bottom: 0,
        position: "absolute",
        width: width
    },
    inputWrapper: {
        marginLeft: 10,
        marginRight: 10,
        marginTop: 0,
        marginBottom: 10,
        paddingLeft: 5,
        paddingRight: 5,
        backgroundColor: "#fff",
        opacity: 0.9,
        borderRadius: 5,
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: "space-between",
        paddingBottom: 10,
    },
    secondInputWrapper: {
        paddingLeft: 5,
        paddingRight: 5,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 5,
        backgroundColor: "#fff",
        opacity: 0.9,
        borderRadius: 5,
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: "space-between"
    },
    inputSearch: {
        fontSize: 14
    },
    label: {
        fontSize: 10,
        fontStyle: "italic",
        marginTop: 7,
        marginBottom: 0
    },
    styleIcon: {
        fontSize: 18, color: "rgba(55,88,119,119)", paddingRight: 5,
    },
    btn: {
        backgroundColor: "rgba(95,86,176,1)",
        marginTop: 5,
        paddingLeft: 10,
        paddingRight: 10,
        alignItems: 'center',justifyContent: "center",
        alignSelf: 'center',
        marginRight: 5,
    },
    btnText: {
        color: "#fff"
    }
};

export default styles;