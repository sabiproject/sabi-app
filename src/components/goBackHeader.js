import React, { Component } from "react";

import { Header, Body, Title, Left, Icon, Right, Button, View } from "native-base";

class GoBackHeader extends React.Component<Props, State> {
    render() {
        return (
            <Header style={{ height: this.props.height, justifyContent: 'flex-start', flexDirection: 'column', }}>
                <View >
                    <Button transparent>
                        <Icon
                            style={{ color: "white" }}
                            active
                            name="arrow-left"
                            onPress={() => {this.props.goBack()}}
                        />
                    </Button>
                </View>
                <View style={{ alignItems: 'center' }}>
                    <Icon name="bike" style={{ fontSize: 100, color:"white" }}></Icon>
                </View>
            </Header>
        );
    }
}
export default GoBackHeader;
