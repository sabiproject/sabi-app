import React, { Component } from "react";
import { Text, View } from "react-native";
import { Button, Badge } from "native-base";
import DateTimePicker from "react-native-modal-datetime-picker";
import moment from "moment";

export default class TimePicker extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isTimePickerVisible: false,
            selectedTime: moment(this.props.time).format("LT").toString()
        };
    }

    _showTimePicker = () => this.setState({ isTimePickerVisible: true });

    _hideTimePicker = () => this.setState({ isTimePickerVisible: false });

    _handleTimePicked = (date: any) => {
        this.setState({ selectedTime: moment(date).format("LT").toString() });
        this.props.getTime(moment(date).format("LT").toString());
        this._hideTimePicker();
    };

    render() {
        const { isTimePickerVisible, selectedTime } = this.state;
        return (
            <View style={{flexDirection: "row"}}>
                <Button rounded small button warning
                    onPress={this._showTimePicker}
                    style={{ width: 70, justifyContent: "center", height: 20, margin: 0 }}>
                    <Text style={{ color: "white", fontSize: 13, }}>{this.state.selectedTime}</Text>
                </Button>
                <DateTimePicker
                    isVisible={isTimePickerVisible}
                    onConfirm={this._handleTimePicked}
                    onCancel={this._hideTimePicker}
                    mode="time"
                />
            </View>
        );
    }

}
