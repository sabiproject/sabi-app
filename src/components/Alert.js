import React from 'react'
import {
    View,
    Button,
    StyleSheet
} from 'react-native'

import {
    SCLAlert,
    SCLAlertButton
} from 'react-native-scl-alert'

export default class Alert extends React.Component {

    handleOK = () => {
        this.props.handleOK();
    }

    handleClose = () => {
        this.props.handleClose();
    }

    render() {
        return (
            <SCLAlert
                theme={this.props.theme}
                show={this.props.show}
                title={this.props.title}
                subtitle={this.props.subtitle}
                onRequestClose={this.props.onRequestClose}
            >
                {this.props.confirmText &&
                    <SCLAlertButton theme={this.props.theme} onPress={this.handleOK}>{this.props.confirmText}</SCLAlertButton>
                }
                <SCLAlertButton theme={this.props.themeCancel} onPress={this.handleClose}>{this.props.cancelText}</SCLAlertButton>
            </SCLAlert>
        )
    }
}