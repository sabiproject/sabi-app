import React from 'react';
import {
    StyleSheet,
    Dimensions,
    ScrollView,
    View,
    Image
} from 'react-native';

import MapView from 'react-native-maps';
import MapViewDirections from "react-native-maps-directions";
import { mapCustomStyle } from "../container/MapContainer/styles";

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const GOOGLE_MAPS_APIKEY = "AIzaSyDUYbTR-3PDWPhgxjENs4yf35g2eHc641s";

let animationTimeout;
class LiteMapView extends React.Component {
    updateMapView() {
        animationTimeout = setTimeout(
            () => this.mapRef.fitToSuppliedMarkers(
                ["origin", "destination"],
                {
                    edgePadding: {
                        top: 120,
                        right: 10,
                        bottom: 10,
                        left: 10
                    }, animated: true
                }
            ), 0);
    }
    componentDidMount() {
        if (this.props.pickUp && this.props.dropOff
        ) {
            this.updateMapView();
        }
    }
    componentWillUnmount() {
        if (animationTimeout) {
            clearTimeout(animationTimeout);
        }
    }
    render() {
        const { pickUp, dropOff } = this.props;
        const pickUpCoord = { latitude: pickUp.latitude, longitude: pickUp.longitude };
        const dropOffCoord = { latitude: dropOff.latitude, longitude: dropOff.longitude };
        return (
            <View>
                {
                    pickUp && dropOff &&
                    <MapView
                        customMapStyle={mapCustomStyle}
                        provider={MapView.PROVIDER_GOOGLE}
                        key={"1"}
                        ref={(ref) => { this.mapRef = ref }}
                        style={styles.map}
                        liteMode
                        initialRegion={{
                            latitude: pickUp.latitude, longitude: pickUp.longitude, latitudeDelta: LATITUDE_DELTA, longitudeDelta: LONGITUDE_DELTA
                        }}
                    >
                        <MapView.Marker
                            identifier="origin"
                            coordinate={pickUpCoord}
                        >
                            <Image source={require("../../assets/images/pin3.png")} style={{ width: 40, height: 40 }} />
                        </MapView.Marker>
                        <MapView.Marker
                            identifier="destination"
                            coordinate={dropOffCoord}
                        >
                            <Image source={require("../../assets/images/pin4.png")} style={{ width: 40, height: 40 }} />
                        </MapView.Marker>
                        <MapViewDirections
                            origin={pickUpCoord}
                            destination={dropOffCoord}
                            apikey={GOOGLE_MAPS_APIKEY}
                            language="vi"
                            mode="driving"
                            strokeWidth={5}
                            strokeColor="rgba(231,76,60,1)"
                        />
                    </MapView>
                }
            </View>

        );
    }
}

const styles = StyleSheet.create({
    map: {
        height: 200,
    },
});
export default LiteMapView;