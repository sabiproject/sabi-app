import React, { Component } from "react";
import { Text, View } from "react-native";
import { Button, Badge } from "native-base";
import DateTimePicker from "react-native-modal-datetime-picker";
import moment from "moment";

export default class DatePicker extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isDatePickerVisible: false,
            selectedDate: moment(this.props.time).format("L").toString()
        };
    }


    _showDatePicker = () => this.setState({ isDatePickerVisible: true });

    _hideDatePicker = () => this.setState({ isDatePickerVisible: false });

    _handleDatePicked = (date: any) => {
        this.setState({ selectedDate: moment(date).format("L").toString() });
        this.props.getDate(moment(date).format("L").toString());
        this._hideDatePicker();
    };

    render() {
        const { isDatePickerVisible, selectedDate } = this.state;
        return (
            <View style={{flexDirection: "row" }}>
                <Button rounded small button danger
                    onPress={this._showDatePicker}
                    style={{ width: 100, justifyContent: "center", height: 20, margin: 0 }}>
                    <Text style={{ color: "white", fontSize: 13, }}>{this.state.selectedDate}</Text>
                </Button>
                <DateTimePicker
                    isVisible={isDatePickerVisible}
                    onConfirm={this._handleDatePicked}
                    onCancel={this._hideDatePicker}
                />
            </View>
        );
    }

}
