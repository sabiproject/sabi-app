import React, { Component } from "react";

import { Header, Body, Title, Left, Icon, Right, Button } from "native-base";

class CustomHeader extends React.Component<Props, State> {
    render() {
        return (
            <Header>
                <Left>
                    <Button transparent>
                        <Icon
                            active
                            name="menu"
                            onPress={() => this.props.drawerOpen()}
                        />
                    </Button>
                </Left>
                <Body>
                    <Title>{this.props.title}</Title>
                </Body>
                <Right />
            </Header>
        );
    }
}
export default CustomHeader;
