import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    Image
} from "react-native";
import moment from "moment";
import MapView from 'react-native-maps';
import 'moment/locale/vi';
import { Header, Body, Left, Icon, Right, Button, Card, CardItem, Thumbnail, Badge } from "native-base";
import LiteMapView from "./LiteMap";
const status = (status) => {
    if (status == "pending") {
        return (
            <Badge success style={{ width: 100, alignItems: "center" }}>
                <Text style={{ color: "white" }}>Đang đợi</Text>
            </Badge>
        )
    } else {
        <Badge danger style={{ width: 100, alignItems: "center" }}>
            <Text style={{ color: "white" }}>N/A</Text>
        </Badge>
    }
}

class Schedule extends React.Component<Props, State> {
    constructor() {
        super();
    }
    convertDate(datetime) {
        moment.locale("vi");
        return moment(datetime).format("LT") + " " + moment(datetime).format('dddd') + " ngày " + moment(datetime).format("LL")
    }
    getType(type) {
        return type == "bike" ? "Chuyến đi có xe" : "Chuyến đi CHƯA có xe";
    }
    render() {
        const { item } = this.props;
        return (
            <Card>
                <CardItem>
                    <Left>
                        <Thumbnail source={require("../../assets/images/default-avatar.png")} />
                        <Body>
                            <View style={{ flexDirection: 'row', alignItems: 'center', margin: 0 }}>
                                <Text>{item.pickUp.name}</Text>
                                <Icon style={{ fontSize: 15, marginRight: 5, marginLeft: 5 }} name="arrow-right-bold-circle"></Icon>
                                <Text >{item.dropOff.name}</Text>
                            </View>
                            <Text note style={{ fontSize: 10, fontStyle: 'italic' }}>{this.convertDate(item.details.datetime)}</Text>
                            <Text note style={{ fontSize: 10, fontStyle: 'italic' }}>{this.getType(item.type)}</Text>
                        </Body>
                    </Left>
                </CardItem>
                <CardItem cardBody >
                    <View style={styles.LiteMapView}>
                        <LiteMapView dropOff={item.dropOff} pickUp={item.pickUp}></LiteMapView>
                    </View>

                </CardItem>
                <CardItem>
                    <Left>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            {status(item.status)}
                        </View>
                    </Left>
                    <Body>

                    </Body>
                    <Right>
                        <Button transparent style={{ height: 27, margin: 0, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontStyle: 'italic' }}>bởi Nam Trương</Text>
                        </Button>
                    </Right>
                </CardItem>
            </Card>
        );
    }
}
const styles = StyleSheet.create({
    LiteMapView: {
        padding: 0,
        flex: 1
    },
});
export default Schedule;