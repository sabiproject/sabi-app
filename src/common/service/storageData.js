import { AsyncStorage } from "react-native";

export function setItemStorage(key, value) {
    AsyncStorage.setItem(key, value);
}

export async function getItemStorage(key) {
    return await AsyncStorage.removeItem(key);
}

export async function removeItemStorage(key) {
    try {
        await AsyncStorage.removeItem(key);
        return true;
    }
    catch (exception) {
        return false;
    }
}
