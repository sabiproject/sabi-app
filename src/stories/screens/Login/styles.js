import { Dimensions, StyleSheet } from "react-native";
import variables from "./../../../theme/variables/variables (2)";

const deviceHeight = Dimensions.get("window").height;

const styles: any = StyleSheet.create({
	container: {
		flex: 1, justifyContent: "space-between"
	},
	btn: {
		backgroundColor: "rgba(52, 52, 52, 0.8)"
	},
	bgColor: {
	},
	footer: {
		alignItems: "center",
		flexDirection: "row" ,
		justifyContent: "center"
	}
});
export default styles;
