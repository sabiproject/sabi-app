import * as React from "react";
import { Image, Platform, KeyboardAvoidingView, ScrollView, StatusBar } from "react-native";
import { Title, Button, Text, View, Icon } from "native-base";
import LinearGradient from "react-native-linear-gradient";
import Spinner from "../../../components/spinner-laoding";

import { removeItemStorage } from "../../../common/service/storageData";

import styles from "./styles";
export interface Props {
	loginForm: any,
	onLogin: Function,
	loadingVisible: boolean
}
export interface State { }
class Login extends React.Component<Props, State> {
	static navigationOptions: () => ({
		drawerLockMode: 'locked-close',
	});
	render() {
		return (
			<LinearGradient colors={["rgba(95,86,176,1)", "#a26668"]} style={{ flex: 1 }}>
				<KeyboardAvoidingView style={styles.container} keyboardVerticalOffset={-500} behavior={"padding"} enabled>
					<Spinner
						modalVisible={this.props.loadingVisible}
						animationType="fade"
					/>
					<StatusBar barStyle="light-content" backgroundColor="rgba(95,86,176,1)" />


					<View style={{ alignItems: "center", paddingTop: 50 }}>
						<Icon name="bike" style={{ fontSize: 120, color: "white" }} />
						<Title style={{ fontSize: 30, color: "white" }}>Share Bike</Title>
						<View padder>
							<Text style={{ color: Platform.OS === "ios" ? "#000" : "#FFF" }}>
								Ứng dụng chia sẽ xe hoàn toàn miễn phí
							</Text>
						</View>
					</View>
					<View >
						<View padder>
							{this.props.loginForm}
						</View>

						<View padder style={{ flexDirection: "row", justifyContent: "space-between" }}>
							<Button outline rounded style={styles.btn} onPress={() => this.props.on_fbAuth()}>
								<Icon name="facebook" />
								<Text>Facebook</Text>
							</Button>
							<Button outline rounded style={styles.btn} onPress={() => this.props.on_ggAuth()}>
								<Icon name="google" />
								<Text>Google</Text>
							</Button>
						</View>
					</View>
					<View style={{ flexDirection: "column", justifyContent: "flex-end" }}>
						<View style={styles.footer}>
							<View padder>
								<Text style={{ color: "#fff" }}>Phát triển bởi</Text>
							</View>
							<Text style={{ color: "black", fontStyle: "italic", fontWeight: "500", letterSpacing: 10 }}>Viết Nguyễn</Text>
						</View>
					</View>

				</KeyboardAvoidingView>
			</LinearGradient>

		);
	}
}

export default Login;
