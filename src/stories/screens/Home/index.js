import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  List,
  ListItem
} from "native-base";
import { FlatList, View } from "react-native";
import { DrawerActions } from 'react-navigation';
import { BottomNavigation } from "react-native-paper";
import CustomHeader from "../../../components/customHeader";
import AllSchedule from "./All-Schedule/all-schedule";

import ActionButton from 'react-native-action-button';

import styles from "./styles";
export interface Props {
  navigation: any;
  list: any;
}
const MusicRoute = (data) => <AllSchedule data={data} />

const AlbumsRoute = () =>
  <ActionButton buttonColor="rgba(95,86,176,1)">
    <ActionButton.Item buttonColor='#3498db' title="Thêm lịch trình đi nhờ" onPress={() => { }}>
      <Icon name="comment-plus-outline" style={styles.actionButtonIcon} />
    </ActionButton.Item>
    <ActionButton.Item buttonColor='#1abc9c' title="Thêm chia sẽ chuyến đi" onPress={() => { }}>
      <Icon name="map-marker-plus" style={styles.actionButtonIcon} />
    </ActionButton.Item>
  </ActionButton>
  ;

const RecentsRoute = () => <Text>Helllo</Text>;

const Route = () => <Text>Recents</Text>;
export interface State { }
class Home extends React.Component<Props, State> {
  state = {
    index: 0,
    routes: [
      { key: "music", title: "Tất cả", icon: "list", color: "rgba(95,86,176,1)" },
      { key: "albums", title: "Đi nhờ xe", icon: "flight-land", color: "rgba(95,86,176,1)" },
      { key: "recents", title: "Chia sẻ xe", icon: "flight-takeoff", color: "rgba(95,86,176,1)" },
      { key: "purchased", title: "Quanh đây", icon: "person-pin-circle", color: "rgba(95,86,176,1)" }
    ],
    data: this.props.data
  };
  _handleIndexChange = index => this.setState({ index });

  _renderScene = ({ route, data }) => {
    switch (route.key) {
      case 'music':
        return <AllSchedule data={this.props.data} />;
    }
  }

  render() {
    return (
      <Container style={styles.container}>
        <CustomHeader title="Trang chủ" drawerOpen={() => this.props.navigation.dispatch(DrawerActions.toggleDrawer())} />
        {/* <Content>
          <List>
            {this.props.list.map((item, i) => (
              <ListItem
                key={i}
                onPress={() =>
                  this.props.navigation.navigate("BlankPage", {
                    name: { item }
                  })}
              >
                <Text>{item}</Text>
              </ListItem>
            ))}
            <Schedule/>
          </List>
        </Content> */}
        <View>
          <ActionButton buttonColor="rgba(231,76,60,1)">
            <ActionButton.Item buttonColor='#9b59b6' title="New Task" onPress={() => console.log("notes tapped!")}>
              <Icon name="md-create" style={styles.actionButtonIcon} />
            </ActionButton.Item>
            <ActionButton.Item buttonColor='#3498db' title="Notifications" onPress={() => { }}>
              <Icon name="md-notifications-off" style={styles.actionButtonIcon} />
            </ActionButton.Item>
            <ActionButton.Item buttonColor='#1abc9c' title="All Tasks" onPress={() => { }}>
              <Icon name="md-done-all" style={styles.actionButtonIcon} />
            </ActionButton.Item>
          </ActionButton>
        </View>
        <BottomNavigation
          navigationState={this.state}
          onIndexChange={this._handleIndexChange}
          renderScene={this._renderScene}
        />
      </Container>
    );
  }
}

export default Home;
