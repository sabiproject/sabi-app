import * as React from "react";
import { Content, Text } from "native-base";
import { FlatList } from "react-native";

import Schedule from "../../../../components/schedule";

class AllSchedule extends React.Component<Props, State> {
    _renderItem = ({ item }) => (
        <Schedule item={item}></Schedule>
    );
    render() {
        return (
            <Content>
                <FlatList
                    data={this.props.data}
                    renderItem={this._renderItem}
                    keyExtractor={(item, index) => index.toString()}
                />
            </Content>
        )
    }
}
export default AllSchedule;