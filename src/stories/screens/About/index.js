import * as React from "react";
import { Container, Content, Text, Icon, View } from "native-base";

import styles from "./styles";
import GoBackHeader from "../../../components/goBackHeader";
export interface Props {
	navigation: any;
}
export interface State { }
class About extends React.Component<Props, State> {
	constructor(props) {
		super(props);
		console.log(this.props.navigation)
	}
	render() {
		const param = this.props.navigation.state.params;
		return (
			<Container style={styles.container}>
				<GoBackHeader title="Giới thiệu" height={180} goBack={this.props.navigation.goBack} />

				<Content padder>
					<View style={styles.titleView}>
						<Text style={styles.titleText}>ỨNG DỤNG ĐẶT LỊCH TRÌNH VÀ CHIA SẺ XE</Text>
					</View>
					<View >
						<Text style={[styles.text, styles.paddingBottom]}>
							Cảm ơn các bạn đã cài đặt và sữ dụng Sabi. Sabi ra đời với mong muốn giúp những người có nhu cầu cần di chuyển theo lịch trình có sẵn mà có phương tiên di chuyển có thể chia sẽ hành trình đi với người có cùng lịch trình.
						</Text>
						<Text style={[styles.text, styles.paddingBottom]}>
							Khác với các kênh đặt xe khác, Sabi là một ứng dụng HOÀN TOÀN MIỄN PHÍ.
						</Text>
						<Text style={[styles.text, styles.paddingBottom]}>
							Với Sabi, nếu bạn có nhu cầu di chuyển và bạn không có xe. Chỉ đơn giản là bạn chia sẽ lịch trình của bạn với Sabi. Sabi sẽ giúp bạn tìm những tài xế có chia sẽ lịch trình giống bạn.
						</Text>
						<Text style={[styles.text, styles.paddingBottom]}>
							Với Sabi, nếu bạn có một chuyến đi, và đơn giản bạn cần chia sẽ hoặc tìm một người đồng hành. Hãy chia sẽ với chúng tôi, Sabi sẽ giúp bạn tìm bạn đồng hành.
						</Text>
						<Text style={[styles.text]}>
							Nếu gặp khó khăn trong quá trình đặt lịch trình vui lòng liên hệ với Sabi qua Hotline: 096.399.5452.
						</Text>
						<Text style={[styles.text, styles.paddingBottom]}>
							Chúng tôi sẽ hỗ trợ bạn!
						</Text>
						<View style={{ alignItems: 'flex-end' }}>
							<View style={styles.alignCenter}>
								<Text style={[styles.text]}>Thay mặt Sabi team.</Text>
								<Text style={[styles.text]}>Viết Nguyễn</Text>
							</View>

						</View>

					</View>

				</Content>
			</Container>
		);
	}
}

export default About;
