import { StyleSheet } from "react-native";
const primaryColor = "rgba(95,86,176,1)";
const styles: any = StyleSheet.create({
	container: {
		backgroundColor: "#FBFAFA",
	},
	text: {  justifyContent: "space-between",textAlign: "justify", fontSize: 14,fontStyle: 'italic', },
	paddingBot: {paddingBottom: 7},
	alignCenter: { alignItems: 'center' },
	titleView: {
		marginBottom: 10, alignItems: 'center',
	},
	titleText: {
		color: primaryColor,
		fontWeight: "700"

	}
});
export default styles;
