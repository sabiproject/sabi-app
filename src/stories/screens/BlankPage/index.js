import * as React from "react";
import { Container, Header, Title, Content, Text, Button, Icon, Left, Right, Body, View, Card, CardItem } from "native-base";

import CustomHeader from "../../../components/customHeader";
import { Image, StyleSheet, Dimensions } from "react-native";
export interface Props {
	navigation: any;
}
export interface State { }
class BlankPage extends React.Component<Props, State> {

	
	renderStar(star) {
		const fullStar = (index) => <Icon name="star" key={index} style={{ color: "#FD4" }} />
		const halfStar = (index) => <Icon name="star-half" key={index} style={{ color: "#FD4" }} />
		let resultStar = [];
		while (star >= 1) {
			resultStar.push(fullStar(star));
			star = star - 1;
		}
		if (star > 0) {
			resultStar.push(halfStar(0));
		}
		return resultStar;
	}

	renderItem(data) {
		return (
			data.map((x, index) => {
				return (
					<View key={index} style={{ flexDirection: "row" }}>
						<Icon name={x.icon}></Icon>
						<Text>{x.value}</Text>
					</View>
				)
			})
		)
	}
	render() {
		const param = this.props.navigation.state.params;
		const profileInfo = [{ icon: "phone", value: "0963995452" },
		{ icon: "inbox", value: "anhvietcx@gmail.com" },
		{ icon: "facebook", value: "Viet Nguyen" },
		{ icon: "inbox", value: "anhvietcx@gmail.com" }];
		return (
			<Container style={styles.container}>

				<View style={{ height: height / 3, backgroundColor: "rgba(92,168,248,1)" }} >
					<View style={styles.headerProfile}>


						<Image
							style={styles.drawerImage}
							source={require("../../../../assets/images/default-avatar.png")} />
						<Text style={{ textAlign: "center", color: "white", fontWeight: "bold" }}>Viết Nguyễn</Text>
					</View>
				</View>
				<View style={styles.card} padder>
					<Card>
						<CardItem header bordered style={{ flexDirection: "column" }}>

							<View style={{ flexDirection: "row" }}>
								{this.renderStar(4.5)}
							</View>
							<Text>(600 voted)</Text>
						</CardItem>
						<CardItem style={{ justifyContent: "space-between" }}>
							<Text>Số chuyến chia sẽ:</Text>
							<Text>33</Text>
						</CardItem>
						<CardItem style={{ justifyContent: "space-between" }}>
							<Text>Số chuyến được chia sẽ:</Text>
							<Text>69</Text>
						</CardItem>
					</Card>
				</View>
				<View style={{ flex: 2, backgroundColor: "white", alignItems: 'center', justifyContent: 'center' }}>
					<View padder>
						{this.renderItem(profileInfo)}
					</View>
				</View>

			</Container>
		);
	}
}

export default BlankPage;

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;
const styles = StyleSheet.create({
	drawerImage: {
		height: 150,
		width: 150,
		borderRadius: 75,
		borderColor: "white",
		borderWidth: 5,

	},
	card: {
		width: width - 50,
		position: "absolute",
		top: height / 3 - 50,
		right: 25,
	},
	headerProfile: {
		position: "absolute",
		right: width / 2 - 150 / 2,
		top: 10,
		zIndex: 999
	}
});
