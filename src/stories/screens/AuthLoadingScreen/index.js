import React from "react";
import {
    ActivityIndicator,
    AsyncStorage,
    StatusBar,
    StyleSheet,
    View,
} from "react-native";
import { Text, Title } from "native-base";
import LinearGradient from "react-native-linear-gradient";
import * as Progress from 'react-native-progress';

class AuthLoading extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            progress: 0,
            indeterminate: true,
        };

    }

    _bootstrapAsync = async () => {
        setTimeout(async () => {
            const userToken = await AsyncStorage.getItem("userInfo");
            this.props.navigation.navigate(userToken ? "App" : "Auth");
        }, 1000);
    };


    animate() {
        let progress = 0;
        this.setState({ progress });
        setTimeout(() => {
            this.setState({ indeterminate: false });
            setInterval(() => {
                progress += Math.random() / 5;
                if (progress > 1) {
                    progress = 1;
                }
                this.setState({ progress });
            }, 500);
        }, 1500);
    }
    render() {
        return (
            <LinearGradient colors={["rgba(95,86,176,1)", "#a26668"]} style={{ flex: 1 }}>
                <View style={styles.container}>
                    <StatusBar
                        backgroundColor="rgba(95,86,176,1)"
                        barStyle="light-content"
                    />
                    <View >
                        <Title style={{ fontSize: 80 }}>Sabi</Title>
                        <Text style={{ fontSize: 15, color: "white" }}>Ứng dụng chia sẻ - đi nhờ xe hoàn toàn miễn phí</Text>
                    </View>
                    <View style={{ paddingTop: 30 }}>
                        <Progress.Bar width={200} color={"white"} borderColor={"white"}
                            animationType={"timing"} progress={this.state.progress}
                            indeterminate={this.state.indeterminate} 
                            useNativeDriver={true}/>
                    </View>

                </View>
            </LinearGradient>
        );
    }

}
const styles: any = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});
export default AuthLoading;
