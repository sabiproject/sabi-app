import * as React from "react";
import {
	Text, Container, List, Item, ListItem,
	Content, Header, Body, Badge, View, Icon,
	Button, Left, Right
} from "native-base";
import { Image, StyleSheet } from "react-native";
import { NavigationActions, DrawerItems, SafeAreaView } from "react-navigation";
import { commonsStyle } from "../../styles/common";

export interface State { }
export default class Sidebar extends React.Component<Props, State> {
	render() {
		return (
			<Container>
				<Header style={{ height: 200 }}>
					<Body style={commonsStyle.alignCenterParent}>
						<Image
							style={styles.drawerImage}
							source={require("../../../../assets/images/default-avatar.png")} />
						<View padder style={{ flexDirection: "row" }}>
							<Text style={{ color: "white" }}>Xin chào </Text>
							<Badge style={{ backgroundColor: "rgba(52, 52, 52, 0.8)" }}>
								<Text>Viết Nguyễn</Text>
							</Badge>
						</View>


					</Body>
				</Header>
				<Content style={{ flex: 1 }}>
					<SafeAreaView forceInset={{ top: "always", horizontal: "never" }}>
						<DrawerItems {...this.props} />
						<Button full icon block button transparent
							style={{ justifyContent: 'flex-start', }}
							onPress={() => { this.props.signOut() }}>
							<Icon style={{
								color: "rgba(161,168,201,1)",
								fontSize: 24,
								marginLeft: 18,
							}} name="logout" />
							<Text button style={{ fontSize: 14, fontWeight: "500", color: "rgba(0,0,0,0.9)" }}>Đăng xuất</Text>
						</Button >

					</SafeAreaView>

				</Content>
				<View style={{ alignSelf: "stretch", backgroundColor: "rgba(95,86,176,1)" }}>
					<View style={{ alignSelf: "center", }}>
						<Text note style={{ color: "#fff", }}>Sabi version 1.0.0</Text>
					</View>
				</View>
			</Container>
		);
	}
}
const styles = StyleSheet.create({
	drawerImage: {
		height: 150,
		width: 150,
		borderRadius: 75,
		borderColor: "white",
		borderWidth: 5
	},
	active: {
		backgroundColor: "rgba(52, 52, 52, 0.2)"
	},
	btn: {
		height: 30
	}
});
