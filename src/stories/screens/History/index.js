import * as React from "react";
import { Container} from "native-base";
import { View } from "react-native";

import styles from "./styles";
import CustomHeader from "../../../components/customHeader";

import Map from "../../../components/mapComponent/map";
export interface Props {
	navigation: any;
}
export interface State { }
class History extends React.Component<Props, State> {
	render() {
		return (
			<Container style={styles.container}>
				<CustomHeader title="History" drawerOpen={() => this.props.navigation.openDrawer()} />

				<View style={{flex:1,zIndex:1000}}>
					<Map navigation={this.props.navigation}/>
				</View>




			</Container>
		);
	}
}

export default History;
