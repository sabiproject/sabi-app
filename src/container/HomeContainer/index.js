// @flow
import * as React from "react";
import firebase from 'react-native-firebase';
import { connect } from "react-redux";
import Home from "../../stories/screens/Home";
import datas from "./data";
export interface Props {
	navigation: any,
	fetchList: Function,
	data: Object,
}
export interface State { }
class HomeContainer extends React.Component<Props, State> {
	constructor() {
		super();
		this.ref = firebase.firestore().collection("booking");
		this.unsubscribe = null;
		this.state = {
			data: [],
		};
	}
	static navigationOptions = { drawerLockMode: 'unlocked' };
	componentDidMount() {
		//this.props.fetchList(datas);
		// /this.ref = firebase.firestore().collection("booking");
		// this.ref.doc("R1RT8PrbHBjY3RQyxcFN").onSnapshot((snapshot) => {
		// 	console.log(snapshot)
		// }, (error) => {
		// 	console.log(error)
		// });
		this.ref.where("createdUser.uid", "==", "JFY91IdMdJeGOK6yx5n0oodMHDz2").get().then((querySnapshot) => {
			if (querySnapshot.empty) {
				console.log('no documents found');
			} else {
				let data = [];
				querySnapshot.forEach((documentSnapshot) => {
					data.push(documentSnapshot.data());
				});
				this.setState({
					data: data
				})
			}
		});
		//this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate)
	}
	componentWillUnmount() {
		//this.unsubscribe();
	}
	onCollectionUpdate = (querySnapshot) => {
		const listData = [];
		querySnapshot.forEach((doc) => {
			const { createdUser, createdAt } = doc.data();
			listData.push({
				key: doc.id,
				doc, // DocumentSnapshot
				createdUser,
				createdAt,
			});
		});
		console.log(listData);
	}
	render() {
		return <Home navigation={this.props.navigation} data={this.state.data} />;
	}
}
const mapActionCreators = {

}

const mapStateToProps = state => ({
	data: state.homeReducer.list,
	isLoading: state.homeReducer.isLoading,
});
export default connect(mapStateToProps, mapActionCreators)(HomeContainer);
