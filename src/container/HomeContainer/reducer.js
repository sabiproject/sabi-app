import update from "react-addons-update";
const initialState = {
	list: [],
	isLoading: true,
};
function handleFetchList(state, action) {
	return update(state, {
		list: {
			$set: action.payload ? action.payload : []
		},
		isLoading: {
			$set: false
		}
	})
}
export default function (state: any = initialState, action: Function) {
	if (action.type === "FETCH_LIST_SUCCESS") {
		return handleFetchList(state, action)
	}
	if (action.type === "LIST_IS_LOADING") {
		return {
			...state,
			isLoading: action.isLoading,
		};
	}
	return state;
}
