// @flow
import * as React from "react";
import BlankPage from "../../stories/screens/BlankPage";
import { connect } from "react-redux";
export interface Props {
	navigation: any,
}
class BlankPageContainer extends React.Component {

	render() {
		return <BlankPage navigation={this.props.navigation} userInfo={this.props.userInfo}/>;
	}
}
function mapStateToProps(state) {
	return { userInfo: state.userReducer.userInfo._user }
}
export default connect(mapStateToProps)(BlankPageContainer);
