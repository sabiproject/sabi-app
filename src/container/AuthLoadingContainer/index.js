// @flow
import * as React from "react";
import { AsyncStorage } from "react-native";
import { connect } from "react-redux";
import { fetchUserInfoAction } from "../LoginContainer/actions";
import AuthLoading from "../../stories/screens/AuthLoadingScreen";

import firebase from "react-native-firebase";
export interface Props {
	navigation: any,
}
export interface State { }
class AuthLoadingContainer extends React.Component<Props, State> {
	constructor(props) {
		super(props);
		this.unsubscribe = null;
	}
	componentDidMount() {
		this.unsubscribe = firebase.auth().onAuthStateChanged((user) => {
			if (user) {
				this.props.fetchUserInfoAction(user);
				this.props.navigation.navigate("Home");
			} else {
				this.props.navigation.navigate("Login");
			}
		})
	}
	componentWillUnmount() {
		if (this.unsubscribe) this.unsubscribe();
	}
	render() {
		return <AuthLoading navigation={this.props.navigation} />;
	}

}

function bindAction(dispatch) {
	return {
		fetchUserInfoAction: obj => dispatch(fetchUserInfoAction(obj)),
	};
}

const mapStateToProps = state => ({
	userInfo: state.userReducer.userInfo
});
export default connect(mapStateToProps, bindAction)(AuthLoadingContainer);