// @flow
import * as React from "react";
import { AsyncStorage } from "react-native";
import { StackActions, NavigationActions } from 'react-navigation';
import Sidebar from "../../stories/screens/Sidebar";
import firebase from "react-native-firebase";
export interface Props {
	navigation: any,
}
export interface State { }
const resetAction = StackActions.reset({
	index: 0,
	actions: [NavigationActions.navigate({ routeName: 'AuthLoading' })],
});
export default class SidebarContainer extends React.Component<Props, State> {
	signOut = async () => {
		AsyncStorage.clear();
		await firebase.auth().signOut();
		this.props.navigation.navigate("AuthLoading");
	}
	render() {
		return <Sidebar navigation={this.props.navigation} {...this.props} signOut={this.signOut} />;
	}
}
