// @flow
import * as React from "react";
import About from "../../stories/screens/About";
export interface Props {
	navigation: any,
}
export interface State {}
export default class AboutContainer extends React.Component<Props, State> {
	render() {
		return <About navigation={this.props.navigation} />;
	}
}
