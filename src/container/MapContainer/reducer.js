import constants from "./actions";
import update from "react-addons-update";
import { Dimensions } from "react-native";
import RNGooglePlaces from "react-native-google-places";
import firebase from 'react-native-firebase';
import request from "../../common/util/request";

const {
    GET_CURRENT_LOCATION,
    GET_INPUT,
    TOGGLE_SEARCH_RESULT,
    GET_ADDRESS_PREDICTIONS,
    GET_SELECTED_ADDRESS,
    GET_DISTANCE_MATRIX,
    CREATE_SCHEDULE,
    RESET_MAP_REDUCER
} = constants;
const { width, height } = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.003;
const LONGITUDE_DELTA = ASPECT_RATIO * LATITUDE_DELTA;
//-----------------------------------------------
// actions
//-----------------------------------------------
export function getCurrentLocation() {
    return (dispatch) => {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                dispatch({
                    type: GET_CURRENT_LOCATION,
                    payload: position
                });
            },
            (error) => console.log(error.message),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );
    }
}

export function getInputData(payload) {
    return {
        type: GET_INPUT,
        payload
    }
}

export function toogleSearchResult(payload) {
    return {
        type: TOGGLE_SEARCH_RESULT,
        payload
    }
}

export function getAddressPredictions() {
    return (dispatch, store) => {
        let userInput = store().mapReducer.resultTypes.pickUp ? store().mapReducer.inputData.pickUp : store().mapReducer.inputData.dropOff;
        RNGooglePlaces.getAutocompletePredictions(userInput,
            {
                country: "VN"
            }
        )
            .then((results) =>
                dispatch({
                    type: GET_ADDRESS_PREDICTIONS,
                    payload: results
                })
            )
            .catch((error) => console.log(error.message));
    };
}

export function getSelectedAddress(payload) {
    return (dispatch, store) => {
        RNGooglePlaces.lookUpPlaceByID(payload)
            .then((results) => {
                dispatch({
                    type: GET_SELECTED_ADDRESS,
                    payload: results
                })
            })
            .then(() => {
                //Get the distance and time
                if (store().mapReducer.selectedAddress.selectedPickUp && store().mapReducer.selectedAddress.selectedDropOff) {
                    request.get("https://maps.googleapis.com/maps/api/distancematrix/json")
                        .query({
                            origins: store().mapReducer.selectedAddress.selectedPickUp.latitude + "," + store().mapReducer.selectedAddress.selectedPickUp.longitude,
                            destinations: store().mapReducer.selectedAddress.selectedDropOff.latitude + "," + store().mapReducer.selectedAddress.selectedDropOff.longitude,
                            mode: "driving",
                            key: "AIzaSyDUYbTR-3PDWPhgxjENs4yf35g2eHc641s"
                        })
                        .finish((error, res) => {
                            dispatch({
                                type: GET_DISTANCE_MATRIX,
                                payload: res.body
                            });
                        })
                }
            })
            .catch((error) => console.log(error.message))
    }
}
export function createSchedule(payloadFromComponent) {
    return (dispatch, store) => {
        const payload = {
            data: {
                createdUser: {
                    uid: store().userReducer.userInfo.uid,
                    displayName: store().userReducer.userInfo.displayName
                },
                createdAt: Date.now(),
                amended: false,
                pairedWithRef: "",
                pickUp: {
                    address: store().mapReducer.selectedAddress.selectedPickUp.address,
                    name: store().mapReducer.selectedAddress.selectedPickUp.name,
                    latitude: store().mapReducer.selectedAddress.selectedPickUp.latitude,
                    longitude: store().mapReducer.selectedAddress.selectedPickUp.longitude
                },
                dropOff: {
                    address: store().mapReducer.selectedAddress.selectedDropOff.address,
                    name: store().mapReducer.selectedAddress.selectedDropOff.name,
                    latitude: store().mapReducer.selectedAddress.selectedDropOff.latitude,
                    longitude: store().mapReducer.selectedAddress.selectedDropOff.longitude
                },
                status: "pending",
                details: {
                    datetime: payloadFromComponent.datetime,
                    duration: {
                        text: store().mapReducer.distanceMatrix.rows[0].elements[0].duration.text,
                        value: store().mapReducer.distanceMatrix.rows[0].elements[0].duration.value
                    },
                    distance: {
                        text: store().mapReducer.distanceMatrix.rows[0].elements[0].distance.text,
                        value: store().mapReducer.distanceMatrix.rows[0].elements[0].distance.value
                    }
                },
                fare: payloadFromComponent.fare,
                type: payloadFromComponent.typeSharing
            }
        };
        firebase.firestore().collection("booking").add(payload.data).then(x => {
            if (x) {
                dispatch({
                    type: CREATE_SCHEDULE,
                    payload: true
                })
            }
        });

    }
}

export function resetMapReducer() {
    return {
        type: RESET_MAP_REDUCER,
        payload: true
    }
}
//-----------------------------------------------
// action handlers
//-----------------------------------------------
function handleGetCurrentLocation(state, action) {
    return update(state, {
        region: {
            latitude: {
                $set: action.payload.coords.latitude
            },
            longitude: {
                $set: action.payload.coords.longitude
            },
            latitudeDelta: {
                $set: LATITUDE_DELTA
            },
            longitudeDelta: {
                $set: LONGITUDE_DELTA
            }
        }
    })
}
function handleGetInputData(state, action) {
    const { key, value } = action.payload;
    return update(state, {
        inputData: {
            [key]: {
                $set: value
            }
        }
    });
}
function handleToggleSearchResult(state, action) {
    if (action.payload === "pickUp") {
        return update(state, {
            resultTypes: {
                pickUp: {
                    $set: true,
                },
                dropOff: {
                    $set: false
                }
            },
            predictions: {
                $set: {}
            }

        });
    }
    if (action.payload === "dropOff") {
        return update(state, {
            resultTypes: {
                pickUp: {
                    $set: false,
                },
                dropOff: {
                    $set: true
                }
            },
            predictions: {
                $set: {}
            }

        });
    }
}

function handleGetAddressPredictions(state, action) {
    return update(state, {
        predictions: {
            $set: action.payload
        }
    })
}

function handleGetSelectedAddress(state, action) {
    let selectedTitle = state.resultTypes.pickUp ? "selectedPickUp" : "selectedDropOff"
    return update(state, {
        selectedAddress: {
            [selectedTitle]: {
                $set: action.payload
            }
        },
        resultTypes: {
            pickUp: {
                $set: false
            },
            dropOff: {
                $set: false
            }
        }
    })
}

function handleGetDitanceMatrix(state, action) {
    return update(state, {
        distanceMatrix: {
            $set: action.payload
        }
    })
}

function handleCreateSchedule(state, action) {
    if (action.payload) {
        return update(state, {
            result: {
                $set: true
            }
        })
    } else {
        return update(state, {
            result: {
                $set: false
            }
        })
    }
}
function handleResetMapReducer(state, action) {

    return update(state, {
        selectedAddress: {
            $set: {}
        },
        resultTypes: {
            pickUp: {
                $set: false
            },
            dropOff: {
                $set: false
            }
        },
        distanceMatrix: {
            $set: {}
        },
        result: {
            $set: false
        }
    })

}
//-----------------------------------------------
// const
//-----------------------------------------------
const ACTION_HANDLERS = {
    GET_CURRENT_LOCATION: handleGetCurrentLocation,
    GET_INPUT: handleGetInputData,
    TOGGLE_SEARCH_RESULT: handleToggleSearchResult,
    GET_ADDRESS_PREDICTIONS: handleGetAddressPredictions,
    GET_SELECTED_ADDRESS: handleGetSelectedAddress,
    GET_DISTANCE_MATRIX: handleGetDitanceMatrix,
    CREATE_SCHEDULE: handleCreateSchedule,
    RESET_MAP_REDUCER: handleResetMapReducer,
}

const initialState = {
    region: {},
    inputData: {},
    resultTypes: {},
    selectedAddress: {},
    distanceMatrix: {}
};
export default function mapReducer(state: any = initialState, action) {
    console.log(action.type)
    const handler = ACTION_HANDLERS[action.type];
    return handler ? handler(state, action) : state;
}