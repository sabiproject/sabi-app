import React from "react";
import { Image } from "react-native";
import { View, Text } from "native-base";
import MapView from "react-native-maps";
import MapViewDirections from "react-native-maps-directions";

import { styles, mapCustomStyle } from "./styles";
import SearchBox from "../../components/searchBox/searchBox";
import SearchResult from "../../components/searchResult/searchResult";
import BookDialog from "../../components/bookDialog/bookDialog"

const GOOGLE_MAPS_APIKEY = "AIzaSyDUYbTR-3PDWPhgxjENs4yf35g2eHc641s";
export interface Props {
    navigation: any;
    region: any;
    getInputData: any;
    toogleSearchResult: any;
    getAddressPredictions: any;
    resultTypes: any;
    predictions: any;
    getSelectedAddress: any;
    selectedAddress: any;
    distanceMatrix: any;
    createSchedule: any;
    resetMapReducer: any;
    result: boolean
}
export class MapContainer extends React.Component {
    constructor(props) {
        super(props);
        this.mapRef = null;
    }
    updateMapView() {
        setTimeout(
            () => this.mapRef.fitToSuppliedMarkers(
                ["origin", "destination"],
                {
                    edgePadding: {
                        top: 400,
                        right: 20,
                        bottom: 300,
                        left: 20
                    }, animated: true
                }
            ), 1000);
    }
    componentDidMount() {
        if (this.props.selectedAddress &&
            this.props.selectedAddress.selectedPickUp &&
            this.props.selectedAddress.selectedDropOff
        ) {
            this.updateMapView();
        }
    }
    componentDidUpdate(prevProps, prevState) {
        if (this.props.selectedAddress &&
            this.props.selectedAddress != prevProps.selectedAddress &&
            this.props.selectedAddress.selectedPickUp &&
            this.props.selectedAddress.selectedDropOff
        ) {
            this.updateMapView();
        }
    }
    render() {
        const {
            navigation, region, getInputData, toogleSearchResult,
            getAddressPredictions, resultTypes,
            predictions, getSelectedAddress, selectedAddress,
            distanceMatrix, createSchedule, resetMapReducer, result } = this.props;
        return (
            <View style={styles.container}>
                <MapView
                    ref={(ref) => { this.mapRef = ref }}
                    provider={MapView.PROVIDER_GOOGLE}
                    style={styles.map}
                    region={region}
                    customMapStyle={mapCustomStyle}
                >

                    {
                        region && region.latitude &&
                        <MapView.Marker
                            coordinate={{ latitude: region.latitude, longitude: region.longitude }}
                            pinColor="blue"
                        >
                            <Image
                                source={require("../../../assets/images/pin1.png")}
                                style={{ width: 40, height: 40 }}
                            />
                        </MapView.Marker>
                    }

                    {selectedAddress && selectedAddress.selectedPickUp && selectedAddress.selectedDropOff &&
                        <View>
                            <MapView.Marker
                                identifier="origin"
                                coordinate={{ latitude: selectedAddress.selectedPickUp.latitude, longitude: selectedAddress.selectedPickUp.longitude }}
                                pinColor="blue"
                            >
                                {/* <Image
                                    source={require("../../../assets/images/pin3.png")}
                                    style={{ width: 40, height: 40 }}
                                /> */}
                            </MapView.Marker>
                            <MapView.Marker
                                identifier="destination"
                                coordinate={{ latitude: selectedAddress.selectedDropOff.latitude, longitude: selectedAddress.selectedDropOff.longitude }}
                                pinColor="red"
                            >
                                {/* <Image
                                    source={require("../../../assets/images/pin4.png")}
                                    style={{ width: 40, height: 40 }}
                                /> */}
                            </MapView.Marker>
                            <MapViewDirections
                                origin={{ latitude: selectedAddress.selectedPickUp.latitude, longitude: selectedAddress.selectedPickUp.longitude }}
                                destination={{ latitude: selectedAddress.selectedDropOff.latitude, longitude: selectedAddress.selectedDropOff.longitude }}
                                apikey={GOOGLE_MAPS_APIKEY}
                                language="vi"
                                mode="driving"
                                strokeWidth={5}
                                strokeColor="rgba(231,76,60,1)"
                            />
                        </View>

                    }

                </MapView>
                <SearchBox
                    getInputData={getInputData}
                    toogleSearchResult={toogleSearchResult}
                    getAddressPredictions={getAddressPredictions}
                    selectedAddress={selectedAddress}
                />
                {
                    (resultTypes.pickUp || resultTypes.dropOff) && predictions.length > 0 &&
                    <SearchResult predictions={predictions} getSelectedAddress={getSelectedAddress} />
                }
                {
                    !(resultTypes.pickUp || resultTypes.dropOff) && distanceMatrix && distanceMatrix.rows &&
                    < BookDialog navigation={navigation} distanceMatrix={distanceMatrix} createSchedule={createSchedule} resetMapReducer={resetMapReducer} result={result} />
                }
            </View>
        )
    }
}
export default MapContainer;