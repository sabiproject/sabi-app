const initialState = {
	userInfo: {},
	isSignIn: false,
	isLoading: false
};

export default function (state: any = initialState, action: Function) {
	if (action.type === "FETCH_USER_INFO") {
		return {
			...state,
			userInfo: action.userInfo,
		};
	}
	if (action.type === "IS_SIGN_IN") {
		return {
			...state,
			isSignIn: action.isSignIn,
		};
	}
	if (action.type === "IS_LOADING") {
		return {
			...state,
			isLoading: action.isLoading
		}
	}
	return state;
}
