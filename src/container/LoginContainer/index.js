// @flow
import * as React from "react";

import { connect } from "react-redux";
import { fetchUserInfoAction, fetchIsLoading } from "./actions";

import { Field, reduxForm, formValueSelector, state } from "redux-form";

import { Item, Input, Icon, Toast, Form, View } from "native-base";

import Login from "../../stories/screens/Login";
import FBSDK, { LoginManager, AccessToken } from "react-native-fbsdk";

import firebase from "react-native-firebase";
import firebaseapp from "../../config/firebaseConfig";

import { GoogleSignin } from "react-native-google-signin";

import NumberPhoneLoginForm from "./Components/login.components";

import { setItemStorage, getItemStorage, removeItemStorage } from "../../common/service/storageData";


export interface Props {
  navigation: any;
}
export interface State { }
class LoginForm extends React.Component<Props, State> {
  static navigationOptions: () => ({
    drawerLockMode: 'locked-close',
  });
  constructor(props) {
    super(props);
    this.unsubscribe = null;
    this.state = {
      phoneNumber: "",
      isSent: false
    };
  }

  _fbAuth() {
    LoginManager.logInWithReadPermissions(["public_profile", "email"]).then(
      (result) => {
        if (result.isCancelled) {
          this.renderToast("Đăng nhập bằng Facebook thất bại!");
        } else {
          AccessToken.getCurrentAccessToken().then((accessTokenData) => {
            const credential = firebase.auth.FacebookAuthProvider.credential(accessTokenData.accessToken);
            this.signInandRetrieveData(credential);
          }), (error) => {
            console.log("2" + error);
          };
        }
      }
    );
  }

  async _ggAuth() {

    try {
      await GoogleSignin.configure();

      const data = await GoogleSignin.signIn();

      const credential = firebase.auth.GoogleAuthProvider.credential(data.idToken, data.accessToken);
      this.signInandRetrieveData(credential);

    } catch (e) {
      console.error(e);
    }



  }

  renderToast(content) {
    Toast.show({
      text: content,
      duration: 3000,
      position: "top",
      textStyle: { textAlign: "center" }
    });
  }

  _phoneNumberAuth(numberPhone) {
    firebase.auth().verifyPhoneNumber(numberPhone)
      .on("state_changed", (phoneAuthSnapshot) => {
        switch (phoneAuthSnapshot.state) {
          case firebase.auth.PhoneAuthState.CODE_SENT:
            this.setState({ isSent: true });
            this.renderToast("Đã gửi code.");
            this.setState({ verificationId: phoneAuthSnapshot.verificationId });
            break;
          case firebase.auth.PhoneAuthState.ERROR:
            this.renderToast(`Lỗi trong khi thực hiện: ${phoneAuthSnapshot.error}`);
            break;
          case firebase.auth.PhoneAuthState.AUTO_VERIFY_TIMEOUT:
            if (this.state.isSent) {
              this.renderToast("Quá thời gian tự động xác nhận");
            }
            break;
          case firebase.auth.PhoneAuthState.AUTO_VERIFIED:
            const { verificationId, code } = phoneAuthSnapshot;
            const credential = firebase.auth.PhoneAuthProvider.credential(verificationId, code);
            this.signInandRetrieveData(credential);
            break;
        }
      }, (error) => { error }
        , (phoneAuthSnapshot) => {
          this.setState({ verificationId: phoneAuthSnapshot.verificationId });
        });
  }

  confirmCode(value) {
    if (value.verifyCode) {
      const credential = firebase.auth.PhoneAuthProvider.credential(this.state.verificationId, value.verifyCode);
      this.signInandRetrieveData(credential);
    }
  }

  async signInandRetrieveData(credential) {
    this.props.fetchIsLoading(true);
    var result = await firebase.auth().signInAndRetrieveDataWithCredential(credential);
    if (result) {
      const userInfo = result.user;
      this.props.fetchUserInfoAction(userInfo);
      this.props.navigation.navigate("Home");
    } else {
      this.renderToast("Đăng nhập thất bại, vui lòng thử lại");
    }


  }

  login(value) {
    if (value.numberPhone) {
      const numberPhone = value.numberPhone.toString();
      const correctNumberPhone = "+84" + numberPhone.slice(1);
      this.setState({ phoneNumber: correctNumberPhone });
      this._phoneNumberAuth(correctNumberPhone);
    }
  }


  render() {

    return (
      <Login
        loginForm={<NumberPhoneLoginForm isSent={this.state.isSent} onLogin={(values) => { this.login(values); }} onConfirmCode={(values) => this.confirmCode(values)} />}
        onLogin={() => this.login()}
        on_fbAuth={() => this._fbAuth()}
        on_ggAuth={() => this._ggAuth()}
        loadingVisible={this.props.isLoading}
      />
    );
  }
}
const LoginContainer = reduxForm({
  form: "login"
})(LoginForm);
function bindAction(dispatch) {
  return {
    fetchUserInfoAction: obj => dispatch(fetchUserInfoAction(obj)),
    fetchIsLoading: obj => dispatch(fetchIsLoading(obj))
  };
}

const mapStateToProps = state => ({
  userInfo: state.userReducer.userInfo,
  isSignIn: state.userReducer.isSignIn,
  isLoading: state.userReducer.isLoading
});
export default connect(mapStateToProps, bindAction)(LoginContainer);

