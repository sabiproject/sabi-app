export function fetchUserInfo(userInfo: any) {
	return {
		type: "FETCH_USER_INFO",
		userInfo: userInfo,
	};
}
export function isSignIn(bool: boolean) {
	return {
		type: "IS_SIGN_IN",
		isSignIn: bool,
	};
}
export function isLoading(bool: boolean) {
	return {
		type: "IS_LOADING",
		isLoading: bool
	}
}
export function fetchIsLoading(bool: boolean) {
	return dispatch => {
		dispatch(isLoading(bool: boolean));
	}
}
export function fetchUserInfoAction(url: any) {
	return dispatch => {
		dispatch(isLoading(false));
		dispatch(fetchUserInfo(url: any));
		dispatch(isSignIn(true));
	};
}
