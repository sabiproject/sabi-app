import React, { Component } from "react";
import { View, Keyboard } from "react-native";
import { Container, Item, Input, Header, Body, Content, Title, Button, Text, Icon } from "native-base";
import { Field, reduxForm } from "redux-form";


const validate = values => {
  const error = {};
  error.numberPhone = "";
  var enp = values.numberPhone;
  const rule = /(09|08|07|03[0|1|2|3|4|5|6|7|8|9])+([0-9]{7})\b/g;

  if (values.numberPhone === undefined) {
    enp = "";
  }

  if (rule.test(enp) && enp.length == 10) {
    error.numberPhone = "SĐT chưa thỏa mãn";
  }
  if (enp.length < 10 && enp !== "") {
    error.numberPhone = "SĐT quá ngắn";
  }
  if (!enp.includes("0") && enp.length == 10) {
    error.numberPhone = "Thiếu số 0";
  }
  if (enp.length > 10) {
    error.numberPhone = "SĐT quá dài";
  }
  return error;
};
class NumberPhoneLoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSent: false
    };
    this.renderInput = this.renderInput.bind(this);
  }

  renderInput({ input, placeholder, icon, type, meta: { touched, error, warning } }) {
    var hasError = false;
    if (error !== undefined) {
      hasError = true;
    }
    return (
      <View>
        <Item error={hasError} style={{ backgroundColor: "rgba(255, 255, 255, 0.2)", borderRadius: 57, borderWidth: 0, marginBottom: 15 }}>
          <Icon style={{ color: "#fff", paddingLeft: 15 }} active name={icon} />
          <Input {...input} keyboardType="numeric" placeholder={placeholder} style={{ color: "#fff", borderWidth: 0 }} placeholderTextColor="rgba(255, 255, 255, 0.7)" />
          {hasError ? <Text danger>{error}</Text> : <Text />}
        </Item>
      </View>

    );
  }
  render() {
    const { handleSubmit, isSent, onLogin, onConfirmCode } = this.props;

    return (
      <View >
        {!isSent ?
          <View>
            <Field name="numberPhone" icon={"cellphone-android"} placeholder={"Số điện thoại"} component={this.renderInput} />
            <Button block rounded style={{ backgroundColor: "rgba(52, 52, 52, 0.8)" }} onPress={handleSubmit((values) => { onLogin(values); })}>
              <Text>Đăng nhập</Text>
            </Button>
          </View>
          :
          <View>
            <Field name="verifyCode" icon={"forum"} placeholder={"Mã xác nhận"} component={this.renderInput} />
            <Button block rounded style={{ backgroundColor: "rgba(52, 52, 52, 0.8)" }} onPress={handleSubmit((values) => { Keyboard.dismiss(); onConfirmCode(values); })}>
              <Text>Xác nhận mã</Text>
            </Button>
          </View>
        }

      </View>

    );
  }
}
export default reduxForm({
  form: "test",
  validate
})(NumberPhoneLoginForm);
