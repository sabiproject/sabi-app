import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";

import homeReducer from "../container/HomeContainer/reducer";
import userReducer from "../container/LoginContainer/reducer";
import mapReducer from "../container/MapContainer/reducer";

export default combineReducers({
	form: formReducer,
	homeReducer,
	userReducer,
	mapReducer
});
